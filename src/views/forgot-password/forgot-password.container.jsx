import { ForgotPasswordPage } from './forgot-password.component';
import { connect } from 'react-redux';
import { forgotPassword } from './../../api/user/userAction';

const mapDispatchToProps = {
    forgotPassword
};

const mapStateToProps = state => ({
    isLoading: state.userPage.isLoading,
    isSent: state.userPage.isSent
});

export const ForgotPasswordContainer = connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordPage);