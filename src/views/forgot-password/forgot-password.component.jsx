import React from "react";
import {
  Typography,
  Box,
  Button,
  TextField,
  Grid
} from "@mui/material";
import { useForm } from 'react-hook-form';
import { useNavigate } from "react-router-dom";

export const ForgotPasswordPage = (props) => {
  const { isLoading, forgotPassword } = props;
  const { register, handleSubmit, formState: { errors } } = useForm();
  const navigate = useNavigate();

  return (
    <Grid container sx={{ maxWidth: "450px", margin: "auto", pt: 10 }}>
      <Grid item sm={12} sx={{ px: 5 }}>
        <Box>
          <Box>
            <Typography variant="h1"> Forgot password?</Typography>
            <Typography variant="h3" sx={{ mt: 2 }}>
              If you don’t want to reset password <br />
              <span style={{ color: "#939393" }}> You can</span>{" "}
              <span style={{ cursor: "pointer", fontWeight: 500 }} onClick={() => navigate("/login")}> Login here !</span>
            </Typography>
          </Box>
          <Box sx={{ mt: 4 }}>
            <form onSubmit={handleSubmit(forgotPassword)}>
              <TextField
                fullWidth
                label="Username"
                name="userName"
                variant="outlined"
                {...register('userName', { required: true })}
                error={!!errors.userName}
                helperText={errors.userName && 'Please enter a valid username'}
              />
              <Box sx={{ display: "flex", alignItems: "center", flexDirection: "column", mt: 3 }}>
                <Button
                  disabled={isLoading}
                  fullWidth
                  variant="contained"
                  size="large"
                  type="submit"
                  sx={{ borderRadius: "50px" }}
                >
                  <span type="submit"> {isLoading ? "Wait..." : "Submit"}</span>
                </Button>
              </Box>
            </form>
          </Box>
        </Box>
      </Grid>
    </Grid>
  );
};
