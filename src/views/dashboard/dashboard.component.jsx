export function DashboardPage(props) {
  const { loggedUser } = props;

  function onLogout() {
    sessionStorage.removeItem('loggedUser');
    window.location.reload();
  }
  return (
    <div>
      Dashboard ({ loggedUser.fistName })
      <button onClick={() => onLogout()}>Logout</button>
    </div>
  );
}