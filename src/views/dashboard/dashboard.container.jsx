import { DashboardPage } from './dashboard.component';
import { connect } from 'react-redux';

const mapDispatchToProps = {
    
};

const mapStateToProps = state => ({
    loggedUser: state.userPage.loggedUser
});

export const DashboardContainer = connect(mapStateToProps, mapDispatchToProps)(DashboardPage);