import { ResetPasswordPage } from './reset-password.component';
import { connect } from 'react-redux';
// import { userResetPassword, userGoogleResetPassword } from './../../api/user/userAction';

const mapDispatchToProps = {
    // userResetPassword,
    // userGoogleResetPassword
};

const mapStateToProps = state => ({
    isLoading: state.userPage.isLoading,
    isLoggedIn: state.userPage.isLoggedIn,
    loggedUser: state.userPage.loggedUser
});

export const ResetPasswordContainer = connect(mapStateToProps, mapDispatchToProps)(ResetPasswordPage);