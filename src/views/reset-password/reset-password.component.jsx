import React, { useEffect, useState } from "react";
import {
  Typography,
  Box,
  Button,
  TextField,
  Grid,
  InputAdornment
} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { useForm } from 'react-hook-form';
import { useNavigate } from "react-router-dom";

export const ResetPasswordPage = (props) => {
  const { isLoading } = props;
  const [showPassword, setShowPassword] = useState(false);
  const { register, handleSubmit, formState: { errors } } = useForm();
  const navigate = useNavigate();


  return (
    <Grid container sx={{ pt: 10, maxWidth: "450px", margin: "auto" }}>
      <Grid item sm={12} sx={{ px: 5 }}>
        <Box>
          <Box>
            <Typography variant="h1">Reset password</Typography>
            <Typography variant="h3" sx={{ mt: 2 }}>
              If you don’t want to reset your password <br />
              <span style={{ color: "#939393" }}> You can</span>{" "}
              <span style={{ cursor: "pointer", fontWeight: 500 }} onClick={() => navigate("/register")}> Login here !</span>
            </Typography>
          </Box>
          <Box sx={{ mt: 4 }}>
            <form onSubmit={handleSubmit(console.log)}>
              <TextField
                sx={{ mt: 3 }}
                fullWidth
                label="Password"
                name="password"
                variant="outlined"
                type={showPassword ? 'text' : 'password'}
                {...register('password', { required: true, minLength: 8 })}
                error={!!errors.password}
                helperText={errors.password && 'Password must be at least 8 characters long'}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton onClick={() => setShowPassword(!showPassword)} edge="end">
                        {showPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />
              <Box sx={{ display: "flex", alignItems: "center", flexDirection: "column", mt: 3 }}>
                <Button
                  disabled={isLoading}
                  fullWidth
                  variant="contained"
                  size="large"
                  type="submit"
                  sx={{ borderRadius: "50px" }}
                >
                  <span type="submit"> {isLoading ? "Wait..." : "Reset"}</span>
                </Button>
              </Box>
            </form>
          </Box>
        </Box>
      </Grid>
    </Grid>
  );
};
