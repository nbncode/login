import React, { useState, useEffect } from "react";
import {
  Typography,
  Box,
  Button,
  TextField,
  Grid,
  InputAdornment
} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { useForm } from 'react-hook-form';
import { useNavigate } from "react-router-dom";

export const RegisterPage = (props) => {
  const { userRegister, isSaved, isLoading } = props;
  const [showPassword, setShowPassword] = useState(false);
  const { register, handleSubmit, formState: { errors } } = useForm();
  const navigate = useNavigate();

  useEffect(() => {
    if(isSaved) {
      navigate("/login");
    }
  }, [isSaved]);

  return (
    <Grid container sx={{ maxWidth: "500px", margin: "auto", pt: 7 }}>
      <Grid item sm={12} sx={{ px: 5 }}>
        <Box>
          <Box>
            <Typography variant="h1"> Sign up</Typography>
            <Typography variant="h3" sx={{ mt: 2 }}>
              If you have an account already <br />
              <span style={{ color: "#939393" }}> You can</span>{" "}
              <span style={{ cursor: "pointer", fontWeight: 500 }} onClick={() => navigate("/login")}> Login here !</span>
            </Typography>
          </Box>
          <Box sx={{ mt: 4 }}>
            <form onSubmit={handleSubmit(userRegister)}>
              <TextField
                fullWidth
                label="First Name"
                name="firstName"
                variant="outlined"
                {...register('firstName', { required: true })}
                error={!!errors.firstName}
                helperText={errors.firstName && 'Please enter your first name'}
              />
              <TextField
                sx={{ mt: 3 }}
                fullWidth
                label="Last Name"
                name="lastName"
                variant="outlined"
                {...register('lastName', { required: true })}
                error={!!errors.lastName}
                helperText={errors.lastName && 'Please enter your last name'}
              />
              <TextField
                sx={{ mt: 3 }}
                fullWidth
                label="User Name"
                name="userName"
                variant="outlined"
                {...register('userName', { required: true })}
                error={!!errors.userName}
                helperText={errors.userName && 'Please enter your user name'}
              />
              <TextField
                sx={{ mt: 3 }}
                fullWidth
                label="Email"
                name="email"
                variant="outlined"
                {...register('email', { required: true, pattern: /^\S+@\S+$/i })}
                error={!!errors.email}
                helperText={errors.email && 'Please enter a valid email address'}
              />
              <TextField
                sx={{ mt: 3 }}
                fullWidth
                label="Mobile Number"
                name="mobileNumber"
                variant="outlined"
                {...register('mobileNumber', { required: true })}
                error={!!errors.mobileNumber}
                helperText={errors.mobileNumber && 'Please enter your mobile number'}
              />
              <TextField
                sx={{ mt: 3 }}
                fullWidth
                label="Password"
                name="password"
                variant="outlined"
                type={showPassword ? 'text' : 'password'}
                {...register('password', { required: true, minLength: 8 })}
                error={!!errors.password}
                helperText={errors.password && 'Password must be at least 8 characters long'}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton onClick={() => setShowPassword(!showPassword)} edge="end">
                        {showPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />
              <Box sx={{ display: "flex", alignItems: "center", flexDirection: "column", mt: 3 }}>
                <Button
                  disabled={isLoading}
                  fullWidth
                  variant="contained"
                  size="large"
                  type="submit"
                  sx={{ borderRadius: "50px" }}
                >
                  <span type="submit">{isLoading ? "Wait ..." : "Register"}</span>
                </Button>
              </Box>
            </form>
          </Box>
        </Box>
      </Grid>
    </Grid>
  );
};
