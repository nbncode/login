import { RegisterPage } from './register.component';
import { connect } from 'react-redux';
import { userRegister } from './../../api/user/userAction';

const mapDispatchToProps = {
    userRegister
};

const mapStateToProps = state => ({
    isLoading: state.userPage.isLoading,
    isSaved: state.userPage.isSaved
});
export const RegisterContainer = connect(mapStateToProps, mapDispatchToProps)(RegisterPage);