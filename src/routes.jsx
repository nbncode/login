import React from 'react';
import { Routes, Route } from 'react-router-dom';
import { ProtectedRoute } from './App/protected.route';
import { LoginContainer } from './views/login/login.container';
import { RegisterContainer } from './views/register/register.container';
import { DashboardContainer } from './views/dashboard/dashboard.container';
import { ForgotPasswordContainer } from './views/forgot-password/forgot-password.container';
import { ResetPasswordContainer } from './views/reset-password/reset-password.container';

export const RoutePath = () => (
  <Routes>
    <Route path='/' element={<ProtectedRoute><DashboardContainer /></ProtectedRoute>} />
    <Route path='/login' element={<LoginContainer />} />
    <Route path='/register' element={<RegisterContainer />} />
    <Route path="/dashboard" element={<ProtectedRoute><DashboardContainer /></ProtectedRoute>} />
    <Route path="/forgot-password" element={<ForgotPasswordContainer />} />
    <Route path="/reset-password" element={<ResetPasswordContainer />} />
  </Routes>
);
