import * as React from 'react';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import { useLocation } from "react-router-dom";
import { RoutePath } from './../../routes';

export default function AppRoot() {
  return (
    <Box sx={{ display: 'flex' }} >
      <CssBaseline />
      <RoutePath />
    </Box>
  );
}