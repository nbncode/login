import AppRoot from './app-root.component';
import { connect } from 'react-redux';

export const AppRootContainer = connect(null, null)(AppRoot);