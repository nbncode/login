import * as actionTypes from "./userActionType";
import axios from 'axios';
import { checkError, apiTimeout } from './../error';
import { toast } from 'react-toastify';
const { REACT_APP_API_URL } = process.env;

export const userLogin = (data) => {
      return (dispatch) => {
        dispatch(userLoginInit());
        apiTimeout(axios.post(`${REACT_APP_API_URL}/testauth/testLogin`, data))
        .then((response) => {
            setLoginSession(response.data);
            dispatch(userLoginSuccess(response.data));
        })
        .catch(function (error) {
            dispatch(userLoginFailure());
            checkError(error);
        })
    };
};


export const userLoginInit = () => ({
    type: actionTypes.USER_LOGIN
});

export const userLoginFailure = () => ({
    type: actionTypes.USER_LOGIN_FAILURE
});

export const userLoginSuccess = data => ({
    type: actionTypes.USER_LOGIN_SUCCESS,
    payload: { data }
});

export const userRegister = (data) => {
      return (dispatch) => {
        dispatch(userRegisterInit());
        apiTimeout(axios.post(`${REACT_APP_API_URL}/testauth/testSignup`, data))
        .then((response) => {
            toast.success("You are registered successfully. Please login with credentials.", {
                position: toast.POSITION.TOP_RIGHT,
                theme: "colored"
            });
            dispatch(userRegisterSuccess(response.data));
        })
        .catch(function (error) {
            dispatch(userRegisterFailure());
            checkError(error);
        })
    };
};

export const userRegisterInit = () => ({
    type: actionTypes.USER_REGISTER
});

export const userRegisterFailure = () => ({
    type: actionTypes.USER_REGISTER_FAILURE
});

export const userRegisterSuccess = data => ({
    type: actionTypes.USER_REGISTER_SUCCESS,
    payload: { data }
});
//Helper Function
function setLoginSession(data) {
    sessionStorage.setItem("loggedUser", JSON.stringify(data));
}

export const forgotPassword = (params) => {
    return (dispatch) => {
      dispatch(forgotPasswordInit());
      apiTimeout(axios.get(`${REACT_APP_API_URL}/testauth/testforgotPassword`, { params }))
      .then((response) => {
        toast.success("Please check your registered email address to reset the password.", {
            position: toast.POSITION.TOP_RIGHT,
            theme: "colored"
        });
        dispatch(forgotPasswordSuccess(response.data.data));
      })
      .catch(function (error) {
          dispatch(forgotPasswordFailure());
          checkError(error);
      })
  };
};

export const forgotPasswordInit = () => ({
  type: actionTypes.FORGOT_PASSWORD
});

export const forgotPasswordFailure = () => ({
  type: actionTypes.FORGOT_PASSWORD_FAILURE
});

export const forgotPasswordSuccess = data => ({
  type: actionTypes.FORGOT_PASSWORD_SUCCESS,
  payload: { data }
});

export const resetPassword = (params) => {
    return (dispatch) => {
      dispatch(resetPasswordInit());
      apiTimeout(axios.get(`${REACT_APP_API_URL}/testauth/testresetPassword`, { params }))
      .then((response) => {
          dispatch(resetPasswordSuccess(response.data.data));
      })
      .catch(function (error) {
          dispatch(resetPasswordFailure());
          checkError(error);
      })
  };
};

export const resetPasswordInit = () => ({
  type: actionTypes.RESET_PASSWORD
});

export const resetPasswordFailure = () => ({
  type: actionTypes.RESET_PASSWORD_FAILURE
});

export const resetPasswordSuccess = data => ({
  type: actionTypes.RESET_PASSWORD_SUCCESS,
  payload: { data }
});
