import * as actionType from "./userActionType";

const initialState = {
  isLoading: false,
  isSaved: false,
  isLoggedIn: sessionStorage.getItem('loggedUser') !== null ? true : false,
  loggedUser: sessionStorage.getItem('loggedUser') !== null ? JSON.parse(sessionStorage.getItem('loggedUser')) : {},
  isReset: false
};

export const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionType.USER_LOGIN:
            return {
                ...state,
                isLoading: true,
                isLoggedIn: false
            };
        case actionType.USER_LOGIN_FAILURE:
            return {
                ...state,
                isLoading: false,
                isLoggedIn: false
            };
        case actionType.USER_LOGIN_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isLoggedIn: true,
                loggedUser: action.payload ? action.payload.data : {},
            };
        case actionType.USER_REGISTER:
            return {
                ...state,
                isLoading: true,
                isSaved: false
            };
        case actionType.USER_REGISTER_FAILURE:
            return {
                ...state,
                isLoading: false
            };
        case actionType.USER_REGISTER_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isSaved: true
            };
        case actionType.FORGOT_PASSWORD:
            return {
                ...state,
                isLoading: true
            };
        case actionType.FORGOT_PASSWORD_FAILURE:
            return {
                ...state,
                isLoading: false
            };
        case actionType.FORGOT_PASSWORD_SUCCESS:
            return {
                ...state,
                isLoading: false
            };
        case actionType.RESET_PASSWORD:
            return {
                ...state,
                isLoading: true,
                isReset: false
            };
        case actionType.RESET_PASSWORD_FAILURE:
            return {
                ...state,
                isLoading: false
            };
        case actionType.RESET_PASSWORD_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isReset: true
            };
        default:
            return state;
    }
}